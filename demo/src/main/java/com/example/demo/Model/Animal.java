package com.example.demo.Model;

public class Animal {
    private String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Animal(){

    }
    
    public Animal(String name){
        this.name = name ;
    }

    public String toString() {
        return "Animal [name=" + name + "]";
    }

  

    
}
