package com.example.demo.Model;

public class Mammal extends Animal{

    
    public Mammal(){
        super();
    }

    public Mammal(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Mammal [ Animal [name= " + getName() + "]";
    }

    
    
}
